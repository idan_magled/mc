create table users
(
  type tinyint(1) default '0' not null,
  bday date not null,
  email varchar(255) not null,
  fname varchar(255) not null,
  lname varchar(255) not null,
  id int auto_increment
    primary key
)
;



create table contacts
(
  id int not null,
  u_id int null,
  phone varchar(255) null,
  nickname varchar(255) null,
  constraint contacts_id_u_id_pk
  unique (id, u_id)
)
;

create index contacts_users_id_fk
  on contacts (u_id)
;



create table messages
(
  id int null,
  f_uid int null,
  t_uid int null,
  ts timestamp default CURRENT_TIMESTAMP not null
)
;

create index messages_users_id_fk
  on messages (f_uid)
;

create index messages_users_t_id_fk
  on messages (t_uid)
;

INSERT INTO black_mc.users (type, bday, email, fname, lname) VALUES (1, '2017-06-19', 'idanmagled@gmail.com', 'idan', 'magled');
INSERT INTO black_mc.users (type, bday, email, fname, lname) VALUES (1, '2017-06-13', 'gitit@gmail.com', 'gitit', 'ma');
INSERT INTO black_mc.users (type, bday, email, fname, lname) VALUES (0, '2017-06-17', 'alont@gmail.com', 'alon', 't');
INSERT INTO black_mc.users (type, bday, email, fname, lname) VALUES (0, '2017-06-15', 'david@gmail.com', 'david', 'askldj');


INSERT INTO black_mc.contacts (id, u_id, phone, nickname) VALUES (1, 2, '+97246677466', 'gitit');
INSERT INTO black_mc.contacts (id, u_id, phone, nickname) VALUES (1, 3, '+97246677433', 'alon');
INSERT INTO black_mc.contacts (id, u_id, phone, nickname) VALUES (1, 4, '+97246689433', 'davidon');
