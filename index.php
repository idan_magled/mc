<?php
require_once 'connection.php';

$_GET['action']();

function get_user_contacts_by_name()//$uid, $predict)
{
    global $dbh;
    //take its for get temp//
    $uid = $_GET['uid'];
    $predict = $_GET['predict'];

    $sth = $dbh->prepare("SELECT u_id,phone,nickname FROM contacts WHERE id=:id AND nickname LIKE :nickname");
    //You can pass it by ref.
    $sth->bindParam(':id', $uid, PDO::PARAM_INT);
    //can't - cards
    $sth->bindValue(':nickname', "%$predict%", PDO::PARAM_INT);

    $sth->execute();
    echo json_encode($sth->fetchAll(PDO::FETCH_ASSOC));
}

function get_birthday()//$uid)
{
    $uid = $_GET['uid'];
    global $dbh;
    $sth = $dbh->prepare("SELECT u_id,phone,nickname,bday,email,fname,lname FROM contacts
  INNER JOIN users ON users.id=contacts.u_id WHERE contacts.id = 1 AND users.bday < DATE_ADD(CURRENT_DATE, INTERVAL +3 DAY)");

    $sth->bindParam(':id', $uid, PDO::PARAM_INT);
    $sth->execute();

    echo json_encode($sth->fetchAll(PDO::FETCH_ASSOC));
}

// You can call this function by
//http://black-colt.net/mc/index.php?action=get_data_from_api&item_id=13002

//Second exc//
function get_api_data()
{
    $item_id = $_GET['item_id'];
    $depth = $_GET['depth'];

    fetchItems(get_data_from_api($item_id), $depth);
}

function fetchItems($list_data, $depth)
{
    foreach ($list_data as $item) {
        print_r($item);
        $depth--;
        if ($depth > 0) {
            fetchItems(get_data_from_api($item->itemID), $depth);
        }

    }
}

function get_data_from_api($item_id)
{
    $base_url = "http://mycheckrecommender-prod.us-east-1.elasticbeanstalk.com/api/v1/recommend/franco?item=%s";
    return json_decode(file_get_contents(sprintf($base_url, $item_id)));
}